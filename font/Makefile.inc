# $OpenBSD: Makefile.inc,v 1.2 2016/10/02 20:55:09 matthieu Exp $

CONFIGURE_ARGS += --disable-all-encodings --enable-iso8859-1

realinstall:
	exec ${MAKE} install FCCACHE=: MKFONTDIR=: MKFONTSCALE=:

